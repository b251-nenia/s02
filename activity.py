# 1. Accept a year input from the user and determine if it is a leap year or not.
userInput = input("Please input a year\n")

if userInput.isnumeric():
	year = int(userInput)
	if (year % 4 == 0 and year % 100 != 0) or (year % 400 == 0 and year % 100 == 0) and year > 0:
		print(f"{year} is a leap year.")
	else:
		print(f"{year} is not a leap year.")
else:
	print("Invalid input")

# 2. Accept two numbers (row and col) from the user and create a grid of asterisks using the numbers (row and col).
row = int(input("Enter a number of rows\n"))
col = int(input("Enter a number of columns\n"))

a = 1
b = 1
str = ""

while a <= row:
	while b <= col:
		str += "*"
		b += 1
	print(str)
	a += 1
